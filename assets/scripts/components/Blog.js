function fancySquaresLoadMore (argument) {
	
	// console.log(twigRoute);

	let fancySquaresRestUrl = window.$('body').data('rest'),
		$loadMoreButton = window.$('#data-parameters'),
		$archiveLayout = window.$('[data-js="blog"]'),
		loadMoreButtonID = '#data-parameters',
		$loader = window.$('#loader');
		// underScoreTemplate = _.template(jQuery('#fancySquares-posts').html());

	// start click
	$archiveLayout.on('click',loadMoreButtonID,function(){

	
		let $this = $(this),
			pageCount = $(this).attr('data-pageCount'),
			nextPage = parseInt($(this).attr('data-page')) + 1;

		// var getParams = void 0;

		// _.each($loadMoreButton, function (item, key, value) {
		// 	var thisData = window.$(item).data();
		// 	getParams = thisData;
		// });

		

  //       // getParams.pagecount = pageCount;
		// console.log(getParams);

		$loader.fadeIn();
		$loadMoreButton.fadeOut();

		let form_data = new FormData,
			dataValues = window.$(this).data();
			for (var key in dataValues) {
			    // all_values.push([key, data[key]]);
			    // key[pagecount] = pageCount;
			    if (key === 'pagecount'){

			    	dataValues[key] = Number(pageCount);
			    	form_data.append(key, dataValues[key]);
			    	// console.log(key, data[Number(pageCount)+1]);
			    } else {
			    	form_data.append(key, dataValues[key]);
			    }
			    // console.log(key, dataValues[key]);
			}

		axios.post(ajaxurl, form_data)
			.then(function(response){
				// console.log(response);
				// console.log(response.data);

				let postData = response.data;

				if(postData === 'nomore'){
			    	$loadMoreButton.fadeOut();
			    } else {
			    	for(var hp = 0; hp < postData.length; hp++){
						$('[data-js="append-content"]').append(postData[hp].data);
						$this.attr('data-pagecount', Number(pageCount)+1);
						// console.log(postData[hp]);
					}

					$loadMoreButton.fadeIn();
			    }


			    $loader.fadeOut();
			})
			.catch(function (response) {
		        //handle error
		        console.log(response);
		    });

            

	});
	// end click
}


module.exports = {
	fancySquaresLoadMore: fancySquaresLoadMore
};