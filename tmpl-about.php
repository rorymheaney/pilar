<?php
/**
 * Template Name: About
 * Description: About Page
 */
// $start = TimberHelper::start_timer();
$context = Timber::get_context();


Timber::render('pages/about.twig', $context);

// echo TimberHelper::stop_timer( $start);