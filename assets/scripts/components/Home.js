function fancySquaresHomeCarousel (argument) {
	window.$('[data-js="home-carousel"]').owlCarousel({
		loop:true,
	    // margin:10,
	    nav:true,
	    navContainer: '#customNavHome',
	    dots: false,
	    responsive:false,
	    items: 12
	});
}


module.exports = {
	fancySquaresHomeCarousel: fancySquaresHomeCarousel
};