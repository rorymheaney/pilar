function fancySquaresNavHeight (argument) {
	var $navDiv = window.$('[data-js="nav-items"]'),
		$headerMainDiv = window.$('[data-js="header-main"]');

	var dropdowns = window.$(".header__dropdown-menu .submenu");
    var biggestLength = 0;

    dropdowns.each(function(index, el){
        if(window.$(el).find("li").length > biggestLength)
        {
          biggestLength = window.$(el).find("li").length;
        }
    });

    var neededHeight = 125 + (20 * biggestLength);

    $navDiv.mouseenter(function(){
        // hover on, show dropdowns, change height and change brand image

        dropdowns.show();
        $headerMainDiv.stop().animate({
          "height" : neededHeight + "px"
        }, 400);
        /*window.$(".main-logo").stop().hide();
        window.$(".hover-logo").stop().show();*/
    });

    $headerMainDiv.mouseleave(function(){
        // hover off, hide dropdowns, make height 75 and change brand image
        dropdowns.hide();
        $headerMainDiv.stop().animate({
          "height" : "75px"
        }, 400);
    });
}

function getfancySquaresNavHeight(){
	if(Foundation.MediaQuery.atLeast('medium')){
    	fancySquaresNavHeight();
    }


    window.$(window).on('changed.zf.mediaquery', function(event, newSize, oldSize) {
		// newSize is the name of the now-current breakpoint, oldSize is the previous breakpoint
		if(Foundation.MediaQuery.atLeast('medium')) {
			fancySquaresNavHeight();
		} 
	});
}



module.exports = {
	getfancySquaresNavHeight: getfancySquaresNavHeight
};
