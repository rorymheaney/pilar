<?php 

function additional_gallery_settings() {
  ?>

    <script type="text/html" id="tmpl-custom-gallery-setting">
        <span>Style</span>
        <select data-setting="style">
            <option value="default-style">Pick a style</option>
            <option value="normal">3 in a row</option>
            <option value="fifty-fifty">50/50 Style</option>
            <option value="masonry">Masonry</option>
        </select>
    </script>

    <script type="text/javascript">
        jQuery( document ).ready( function() {
            _.extend( wp.media.galleryDefaults, {
                style: 'default-style'
            } );

            wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend( {
                template: function( view ) {
                    return wp.media.template( 'gallery-settings' )( view )
                         + wp.media.template( 'custom-gallery-setting' )( view );
                }
            } );
        } );
    </script>

  <?php
}
add_action( 'print_media_templates', 'additional_gallery_settings' );


//custom slider for gallery
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    

    if($attr['style'] == 'masonry'){

        $output .= "<div class=\"post-gallery post-gallery--{$attr['style']}\">\n";
        $x = 0;
        $z = 0;
        $y = 0;
        // Now you loop through each attachment
        foreach ($attachments as $id => $attachment) {
            // Fetch the thumbnail (or full image, it's up to you)
    //      $img = wp_get_attachment_image_src($id, 'medium');
        	$count = $x++;
        	// $countB = $z++;
        	$countC = $y++;

    //        $imgThumbnail = wp_get_attachment_image_src($id, 'thumbnail');
            $img = wp_get_attachment_image_src($id, 'full');
            // print_r($id);
            
            if($count == 3){
            	$output .="<div class=\"post-gallery__block post-gallery__block--special\">\n";
            }

            $output .= "<div class=\"post-gallery__block post-gallery__block--{$countC}\">\n";
            $output .= "<div data-open=\"modal-{$id}\" class=\"post-gallery__bg\" style=\"background-image:url({$img[0]})\">\n";
            // $output .= "<img src=\"{$img[0]}\">";
            $output .= "</div>\n";
            $output .= "</div>\n";
            $output .= "<div class=\"reveal\" id=\"modal-{$id}\" data-reveal>\n";
            $output .= "<img src=\"{$img[0]}\">";
            $output .= 	"<button class=\"close-button\" data-close aria-label=\"Close modal\" type=\"button\">\n";
			$output .= "<span aria-hidden=\"true\">&times;</span>\n";
			$output .= "</button>\n";
            $output .= "</div>\n";

            if($count == 4){
            	$output .= "</div>\n";
            }
        }


        $output .= "</div>\n";

    }  
    if ($attr['style'] == 'fifty-fifty'){
        //$output .= "<div class=\"preloader\"></div>\n";
        $output .= "<div class=\"post-gallery post-gallery--{$attr['style']}\">\n";

        // Now you loop through each attachment
        foreach ($attachments as $id => $attachment) {

            $img = wp_get_attachment_image_src($id, 'full');
            $imgPreview = wp_get_attachment_image_src($id, 'cat-lrg-grid');

            $output .= "<div class=\"post-gallery__block\">\n";
            $output .= "<div data-open=\"modal-{$id}\" class=\"post-gallery__bg\" style=\"background-image:url({$imgPreview[0]})\">\n";
            // $output .= "<img src=\"{$img[0]}\">";
            $output .= "</div>\n";
            $output .= "</div>\n";
            $output .= "<div class=\"reveal\" id=\"modal-{$id}\" data-reveal>\n";
            $output .= "<img src=\"{$img[0]}\">";
            $output .=  "<button class=\"close-button\" data-close aria-label=\"Close modal\" type=\"button\">\n";
            $output .= "<span aria-hidden=\"true\">&times;</span>\n";
            $output .= "</button>\n";
            $output .= "</div>\n";
        }


        $output .= "</div>\n";
    } 

    if ($attr['style'] == 'normal'){

        //$output .= "<div class=\"preloader\"></div>\n";
        $output .= "<div class=\"post-gallery post-gallery--normal\">\n";

        // Now you loop through each attachment
        foreach ($attachments as $id => $attachment) {

            $img = wp_get_attachment_image_src($id, 'full');
            $imgPreview = wp_get_attachment_image_src($id, 'cat-lrg-grid');

            $output .= "<div class=\"post-gallery__block\">\n";
            $output .= "<div data-open=\"modal-{$id}\" class=\"post-gallery__bg\" style=\"background-image:url({$imgPreview[0]})\">\n";
            // $output .= "<img src=\"{$img[0]}\">";
            $output .= "</div>\n";
            $output .= "</div>\n";
            $output .= "<div class=\"reveal\" id=\"modal-{$id}\" data-reveal>\n";
            $output .= "<img src=\"{$img[0]}\">";
            $output .=  "<button class=\"close-button\" data-close aria-label=\"Close modal\" type=\"button\">\n";
            $output .= "<span aria-hidden=\"true\">&times;</span>\n";
            $output .= "</button>\n";
            $output .= "</div>\n";
        }


        $output .= "</div>\n";

    }

    

    return $output; 

}

/**
 * HTML Wrapper - Support for a custom class attribute in the native gallery shortcode
 *
 * @param string $html
 * @param array $attr
 * @param int $instance
 *
 * @return $html
 */
// function customize_gallery_abit( $html, $attr, $instance ) {

//     if( isset( $attr['style'] ) && $style = $attr['style'] ) {
//         // Unset attribute to avoid infinite recursive loops
//         unset( $attr['style'] ); 

//         // Our custom HTML wrapper
//         $html = sprintf( 
//             '<div class="custom-img-wrapper wpse-gallery-wrapper-%s">%s</div>',
//             esc_attr( $style ),
//             gallery_shortcode( $attr )
//         );
//     }

//     return $html;
// }
// add_filter( 'post_gallery', 'customize_gallery_abit', 10, 3 );
