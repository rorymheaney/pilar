// for ie
import "babel-polyfill";
// foundation zurb js
import 'foundation-sites';
// owl
import 'owl.carousel';

// window.$ = window.jQuery = jQuery;
// jquery
window.$ = window.jQuery = require("jquery");


// images loaded
// enque on pages if needed
// masonry
// enque on pages if needed

// axios
window.axios = require('axios');

// pages
import './pages.js'
