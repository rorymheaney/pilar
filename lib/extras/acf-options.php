<?php

// add ACF for global calls
// {{options.your_field_bro}}

add_filter( 'timber_context', 'fancySquares_acf_options_timber_context'  );

function fancySquares_acf_options_timber_context( $context ) {
    $context['options'] = get_fields('option');
    return $context;
}


 

// class MySitePost extends TimberImage {

//     public function init( $iid = false){
//         parent::init($iid);

//         if ( $this->is_cropped($iid)){
//            $this->init_with_url($iid['url']);
//         }
//     }

//     public function is_cropped($iid){
//         return is_array($iid) && isset($iid['original_image']);
//     }
// }

//  function add_to_twig( $twig ) {
//     $twig->addFunction(new Twig_SimpleFunction( 'MadeImage', function( $post_id ) {
//                     return new MadeImage( $post_id );
//                 } )
//             );
//    return $twig;
// }