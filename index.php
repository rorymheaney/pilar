<?php
/**
 * The main template file
 *
 * @package  WordPress
 * @subpackage  SageTimber
 * @since  SageTimber 0.1
 */

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();
$templates = array( 'pages/index.twig' );
if ( is_home() ) {
	array_unshift( $templates, 'pages/home.twig' );
}

if( is_category()){
	// Get all categories assigned to post

// $categories = $post->terms( 'category' );

// // Get only the first category from the array
// $context['category_specific'] = reset( $categories );

// $context['term_page'] = new TimberTerm();
}
	// $context[ 'category_specific' ] = Timber::get_term(['taxonomy'=>'category']);

Timber::render( $templates, $context );