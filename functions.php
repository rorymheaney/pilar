<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
    'lib/timber.php',    // Timber setup
    'lib/assets.php',    // Scripts and stylesheets
    'lib/extras.php',    // Custom functions
    'lib/setup.php',     // Theme setup
    'lib/titles.php',    // Page titles
    'lib/customizer.php', // Theme customizer
    'lib/extras/acf-options.php', //acf options
    'lib/api/instagram.php',  // insta api, get latest images'
    'lib/posts/better-comments.php', //new comment templates list 
    'lib/posts/comment-form.php', // comment form
    'lib/posts/custom-gallery.php', // change gallery markup
    'lib/api/better-featured-img.php', // featured image to api
    'lib/api/better-taxonomies.php', // update taxonomies
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

add_action( 'wp_enqueue_scripts', 'fancySquresRosie_enqueue_scripts' );

function fancySquresRosie_enqueue_scripts() {
    wp_enqueue_script( 'wp-util' );
}

add_filter( 'gform_confirmation_anchor_1', '__return_false' );


function add_cat_to_context( $data ){
  $data['cat_id'] = get_query_var('cat');
  // $data['cat_name'] = get_cat_name(get_query_var('cat'));
  return $data;
}
add_filter( 'timber_context', 'add_cat_to_context' );



add_action('wp_ajax_more_all_posts', __NAMESPACE__ . '\\ajax_more_all_posts');
add_action('wp_ajax_nopriv_more_all_posts', __NAMESPACE__ . '\\ajax_more_all_posts');
function ajax_more_all_posts()
{ 

    $cat_id = $_REQUEST[ 'cat' ] or $_GET[ 'cat' ];
    $paged = $_REQUEST['pagecount'];
    $cat = $_REQUEST['dataCat'];


    $postCount = $_POST['posts_per_page'];

    

    $args = [
        'cat' => $cat_id,
        'posts_per_page' => $postCount,
        'post_status' => 'publish'
    ];

    if($paged != ""){
        $args['paged'] = $paged; 
    }

    $posts = Timber::get_posts($args);
    $message = [];
    if($posts){
        foreach($posts as $post){
            $response = array(
                'data' => Timber::compile('templates/blocks/content.twig', ['post' => $post])
            );
            $message[] = $response;
        }
    } else {
        $message = "nomore";
    }
    wp_reset_query();
    wp_reset_postdata();

    wp_send_json($message);
    die();
}